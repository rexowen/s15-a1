function gradeConverter(grade){
	if(grade >= 80){
		console.log('Your grade is A');
	}else if (grade >= 75){
		console.log('Your grade is B+');
	}else if (grade >= 65){
		console.log('Your grade is B');
	}else if (grade >= 55){
		console.log('Your grade is C+');
	}else if (grade >= 50){
		console.log('Your grade is C');
	}else if (grade >= 40){
		console.log('Your grade is D');
	}else if (grade >= 39){
		console.log('Your grade is F');
	}else if (grade <= -1){
		console.log('Your grade is F');
	}else if (grade == 0){
		console.log('Your grade is F');
	}else {
		console.log('Please enter a number')
	}
}

gradeConverter(101);